﻿using System;
using ProyectoFarmacia.UI.Escritorio.Repositorios;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectoFarmacia.UI.Escritorio
{ 
    /// <summary>
    /// Lógica de interacción para AgregarClientes.xaml
    /// </summary>
    public partial class AgregarClientes : Window
    {
        RepositorioClientes repositorio;
        bool esNuevo;
        public AgregarClientes()
        {
            InitializeComponent();
            repositorio = new RepositorioClientes();
            HabilitarCajas(false);
            HabilitarBotones(true);
            ActualizarTabla();
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombre.Clear();
            txbApellidos.Clear();
            txbDireccion.Clear();
            txbRFC.Clear();
            txbTelefono.Clear();
            txbCorreo.Clear();            
            txbNombre.IsEnabled = habilitadas;
            txbApellidos.IsEnabled = habilitadas;
            txbDireccion.IsEnabled = habilitadas;
            txbRFC.IsEnabled = habilitadas;
            txbTelefono.IsEnabled = habilitadas;
            txbCorreo.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregar.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerClientes().Count == 0)
            {
                MessageBox.Show("Aun no hay clientes registrados ", "No hay clientes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgClientes.SelectedItem != null)
                {
                    Cliente a = dtgClientes.SelectedItem as Cliente;
                    HabilitarCajas(true);
                    txbNombre.Text = a.Nombre;
                    txbApellidos.Text = a.Apellido;
                    txbDireccion.Text = a.Direccion;
                    txbRFC.Text = a.RFC;
                    txbTelefono.Text = a.Telefono;
                    txbCorreo.Text = a.Correo;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("Seleccione el Cliente deseado", "Clientes", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerClientes().Count == 0)
            {
                MessageBox.Show("Aun no hay clientes registrados...", "No existe algun cliente", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgClientes.SelectedItem != null)
                {
                    Cliente a = dtgClientes.SelectedItem as Cliente;
                    if (MessageBox.Show("Realmente deseas eliminar a " + a.Nombre + "?", "Eliminar..!!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarCliente(a))
                        {
                            MessageBox.Show("El cliente ha sido removido", "Clientes", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar Cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selecciona un Cliente para ser eliminado", "Eliminar Cliente", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombre.Text) || string.IsNullOrEmpty(txbDireccion.Text) || string.IsNullOrEmpty(txbTelefono.Text) || string.IsNullOrEmpty(txbApellidos.Text))
            {
                MessageBox.Show("Faltan datos del Cliente (Asegurese de llenar todos los campos)", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Cliente a = new Cliente()
                {
                    Nombre = txbNombre.Text,
                    Apellido = txbApellidos.Text,
                    Direccion = txbDireccion.Text,
                    RFC = txbRFC.Text,
                    Telefono = txbTelefono.Text,
                    Correo = txbCorreo.Text,
                };
                if (repositorio.AgregarCliente(a))
                {
                    MessageBox.Show("Cliente Guardado Exitosamente", "Clientes", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar al Cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Cliente original = dtgClientes.SelectedItem as Cliente;
                Cliente a = new Cliente();
                a.Nombre = txbNombre.Text;
                a.Apellido = txbApellidos.Text;
                a.Direccion = txbDireccion.Text;
                a.RFC = txbRFC.Text;
                a.Telefono = txbTelefono.Text;
                a.Correo = txbCorreo.Text;
                if (repositorio.ModificarCliente(original, a))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("El Cliente ha sido actualizado", "Clientes", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar el Cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ActualizarTabla()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = repositorio.LeerClientes();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
