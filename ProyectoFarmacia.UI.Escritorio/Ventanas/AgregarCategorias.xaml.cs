﻿using System;
using ProyectoFarmacia.UI.Escritorio.Repositorios;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectoFarmacia.UI.Escritorio
{
    /// <summary>
    /// Lógica de interacción para AgregarProductos.xaml
    /// </summary>
    public partial class AgregarProductos : Window
    {
        RepositorioCategorias repositorio;
        bool esNuevo;
        public AgregarProductos()
        {
            InitializeComponent();
            repositorio = new RepositorioCategorias();
            HabilitarCajas(false);
            HabilitarBotones(true);
            ActualizarTabla();
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreCategoria.Clear();
            txbNombreCategoria.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregarCategoria.IsEnabled = habilitados;
            btnEliminarCategoria.IsEnabled = habilitados;
            btnEditarCategoria.IsEnabled = habilitados;
            btnGuardarCategoria.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void btnAgregarCategoria_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnEditarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerCategorias().Count == 0)
            {
                MessageBox.Show("Aun no hayCategorias registradas ", "No hay Categorias", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgCategorias.SelectedItem != null)
                {
                    Categorias a = dtgCategorias.SelectedItem as Categorias;
                    HabilitarCajas(true);
                    txbNombreCategoria.Text = a.NombreCategoria;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("Seleccione la Categoria deseada", "Categorias", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEliminarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerCategorias().Count == 0)
            {
                MessageBox.Show("Aun no hay Categorias registradas...", "No existe alguna Categoria", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgCategorias.SelectedItem != null)
                {
                    Categorias a = dtgCategorias.SelectedItem as Categorias;
                    if (MessageBox.Show("Realmente deseas eliminar " + a.NombreCategoria + "?", "Eliminar..!!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarCategoria(a))
                        {
                            MessageBox.Show("La Categoria ha sido removida", "Categorias", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar Categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selecciona una Categoria para ser eliminada", "Categorias", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnGuardarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreCategoria.Text))
            {
                MessageBox.Show("Faltan datos de la Categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Categorias a = new Categorias()
                {
                    NombreCategoria = txbNombreCategoria.Text,
                };
                if (repositorio.AgregarCategoria(a))
                {
                    MessageBox.Show("Categoria Guardada Exitosamente", "Categorias", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar Categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Categorias original = dtgCategorias.SelectedItem as Categorias;
                Categorias a = new Categorias();
                a.NombreCategoria = txbNombreCategoria.Text;
                if (repositorio.ModificarCategoria(original, a))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("La Categoria ha sido actualizada correctamente", "Categorias", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar la Categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ActualizarTabla()
        {
            dtgCategorias.ItemsSource = null;
            dtgCategorias.ItemsSource = repositorio.LeerCategorias();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
