﻿using System;
using ProyectoFarmacia.UI.Escritorio.Repositorios;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectoFarmacia.UI.Escritorio
{
    /// <summary>
    /// Lógica de interacción para AgregarEmpleados.xaml
    /// </summary>
    public partial class AgregarEmpleados : Window
    {
        RepositorioEmpleados repositorio;
        bool esNuevo;
        public AgregarEmpleados()
        {
            InitializeComponent();
            repositorio = new RepositorioEmpleados();
            HabilitarCajas(false);
            HabilitarBotones(true);
            ActualizarTabla();
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreEmpleado.Clear();
            txbApellidoEmpleado.Clear();
            txbPuestoEmpleado.Clear();
            txbNombreEmpleado.IsEnabled = habilitadas;
            txbApellidoEmpleado.IsEnabled = habilitadas;
            txbPuestoEmpleado.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnAgregarEmpleado.IsEnabled = habilitados;
            btnEditarEmpleado.IsEnabled = habilitados;
            btnEliminarEmpleado.IsEnabled = habilitados;
            btnGuardarEmpleado.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void btnAgregarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }       

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerEmpleados().Count == 0)
            {
                MessageBox.Show("Aun no hay empleados registrados ", "No hay empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgEmpleados.SelectedItem != null)
                {
                    Empleados a = dtgEmpleados.SelectedItem as Empleados;
                    HabilitarCajas(true);
                    txbNombreEmpleado.Text = a.Nombre;
                    txbApellidoEmpleado.Text = a.Apellido;
                    txbPuestoEmpleado.Text = a.Puesto;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("Seleccione el empleado deseado", "Empleados", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerEmpleados().Count == 0)
            {
                MessageBox.Show("Aun no hay empleados registrados...", "No existe algun empleado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgEmpleados.SelectedItem != null)
                {
                    Empleados a = dtgEmpleados.SelectedItem as Empleados;
                    if (MessageBox.Show("Realmente deseas eliminar a " + a.Nombre + "?", "Eliminar..!!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarEmpleado(a))
                        {
                            MessageBox.Show("El empleado ha sido removido", "Empleados", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Selecciona un empleado para ser eliminado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreEmpleado.Text) || string.IsNullOrEmpty(txbApellidoEmpleado.Text) || string.IsNullOrEmpty(txbPuestoEmpleado.Text))
            {
                MessageBox.Show("Faltan datos del empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Empleados a = new Empleados()
                {
                    Nombre = txbNombreEmpleado.Text,
                    Apellido = txbApellidoEmpleado.Text,
                    Puesto = txbPuestoEmpleado.Text,
                };
                if (repositorio.AgregarEmpleado(a))
                {
                    MessageBox.Show("Empleado Guardado Exitosamente", "Empleados", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar Empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleados original = dtgEmpleados.SelectedItem as Empleados;
                Empleados a = new Empleados();
                a.Nombre = txbNombreEmpleado.Text;
                a.Apellido = txbApellidoEmpleado.Text;
                a.Puesto = txbPuestoEmpleado.Text;
                if (repositorio.ModificarEmpleado(original, a))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("El Empleado ha sido actualizado", "Empleados", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar el empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ActualizarTabla()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = repositorio.LeerEmpleados();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
    }
}
