﻿using ProyectoFarmacia.UI.Escritorio.Herramientas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFarmacia.UI.Escritorio.Repositorios
{
    class RepositorioEmpleados
    {
        ManejadorDeArchivos archivoEmpleados;
        List<Empleados> Empleadoss;
        public RepositorioEmpleados()
        {
            archivoEmpleados = new ManejadorDeArchivos("Empleados.txt");
            Empleadoss = new List<Empleados>();
        }

        public bool AgregarEmpleado(Empleados empleados)
        {
            Empleadoss.Add(empleados);
            bool resultado = ActualizarArchivo();
            Empleadoss = LeerEmpleados();
            return resultado;
        }

        public bool EliminarEmpleado(Empleados empleados)
        {
            Empleados temporal = new Empleados();
            foreach (var item in Empleadoss)
            {
                if (item.Nombre == empleados.Nombre && item.Apellido == empleados.Apellido && item.Puesto == empleados.Puesto)
                {
                    temporal = item;
                }
            }
            Empleadoss.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Empleadoss = LeerEmpleados();
            return resultado;
        }

        public bool ModificarEmpleado(Empleados original, Empleados modificado)
        {
            Empleados temporal = new Empleados();
            foreach (var item in Empleadoss)
            {
                if (original.Nombre == item.Nombre&&original.Apellido==item.Apellido&&original.Puesto==item.Puesto)
                {
                    temporal = item;
                }
            }
            temporal.Nombre = modificado.Nombre;
            temporal.Apellido = modificado.Apellido;
            temporal.Puesto = modificado.Puesto;
            bool resultado = ActualizarArchivo();
            Empleadoss = LeerEmpleados();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Empleados item in Empleadoss)
            {
                datos += string.Format("{0}|{1}|{2}\n", item.Nombre, item.Apellido,item.Puesto);
            }
            return archivoEmpleados.Guardar(datos);
        }
        public List<Empleados> LeerEmpleados()
        {
            string datos = archivoEmpleados.Leer();
            if (datos != null)
            {
                List<Empleados> empleados = new List<Empleados>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length - 1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Empleados a = new Empleados()
                    {
                        Nombre = campos[0],
                        Apellido = campos[1],
                        Puesto= campos[2],
                    };
                    empleados.Add(a);
                }
                Empleadoss = empleados;
                return empleados;
            }
            else
            {
                return null;
            }
        }
    }
}
