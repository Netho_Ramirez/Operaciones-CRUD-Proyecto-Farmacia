﻿using System;
using System.Collections.Generic;
using ProyectoFarmacia.UI.Escritorio.Herramientas;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFarmacia.UI.Escritorio.Repositorios
{
    class RepositorioCategorias
    {
        ManejadorDeArchivos archivoCategorias;
        List<Categorias> Categoria;
        public RepositorioCategorias()
        {
            archivoCategorias = new ManejadorDeArchivos("Categorias.txt");
            Categoria = new List<Categorias>();
        }

        public bool AgregarCategoria(Categorias categorias)
        {
            Categoria.Add(categorias);
            bool resultado = ActualizarArchivo();
            Categoria = LeerCategorias();
            return resultado;
        }

        public bool EliminarCategoria(Categorias categorias)
        {
            Categorias temporal = new Categorias();
            foreach (var item in Categoria)
            {
                if (item.NombreCategoria == categorias.NombreCategoria)
                {
                    temporal = item;
                }
            }
            Categoria.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Categoria = LeerCategorias();
            return resultado;
        }

        public bool ModificarCategoria(Categorias original, Categorias modificado)
        {
            Categorias temporal = new Categorias();
            foreach (var item in Categoria)
            {
                if (original.NombreCategoria == item.NombreCategoria)
                {
                    temporal = item;
                }
            }
            temporal.NombreCategoria = modificado.NombreCategoria;            
            bool resultado = ActualizarArchivo();
            Categoria = LeerCategorias();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Categorias item in Categoria)
            {
                datos += string.Format("{0}|\n", item.NombreCategoria);
            }
            return archivoCategorias.Guardar(datos);
        }
        public List<Categorias> LeerCategorias()
        {
            string datos = archivoCategorias.Leer();
            if (datos != null)
            {
                List<Categorias> categoria = new List<Categorias>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length - 1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Categorias a = new Categorias()
                    {
                        NombreCategoria = campos[0],                        
                    };
                    categoria.Add(a);
                }
                Categoria = categoria;
                return categoria;
            }
            else
            {
                return null;
            }
        }
    }
}