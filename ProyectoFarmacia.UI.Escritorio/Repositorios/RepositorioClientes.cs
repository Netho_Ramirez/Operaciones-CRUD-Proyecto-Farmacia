﻿using System;
using ProyectoFarmacia.UI.Escritorio.Herramientas;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFarmacia.UI.Escritorio.Repositorios
{
    class RepositorioClientes
    {
        ManejadorDeArchivos archivoClientes;
        List<Cliente> Clientes;
        public RepositorioClientes()
        {
            archivoClientes = new ManejadorDeArchivos("Clientes.txt");
            Clientes = new List<Cliente>();
        }

        public bool AgregarCliente(Cliente cliente)
        {
            Clientes.Add(cliente);
            bool resultado = ActualizarArchivo();
            Clientes = LeerClientes();
            return resultado;
        }

        public bool EliminarCliente(Cliente cliente)
        {
            Cliente temporal = new Cliente();
            foreach (var item in Clientes)
            {
                if (item.Nombre == cliente.Nombre && item.Apellido == cliente.Apellido && item.Direccion == cliente.Direccion && item.RFC == cliente.RFC && item.Telefono == cliente.Telefono && item.Correo == cliente.Correo)
                {
                    temporal = item;
                }
            }
            Clientes.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Clientes = LeerClientes();
            return resultado;
        }

        public bool ModificarCliente(Cliente original, Cliente modificado)
        {
            Cliente temporal = new Cliente();
            foreach (var item in Clientes)
            {
                if (original.Nombre == item.Nombre && original.Apellido == item.Apellido && original.Direccion == item.Direccion && original.RFC == item.RFC && original.Telefono == item.Telefono && original.Correo == item.Correo)
                {
                    temporal = item;
                }
            }
            temporal.Nombre = modificado.Nombre;
            temporal.Apellido = modificado.Apellido;
            temporal.Direccion = modificado.Direccion;
            temporal.RFC = modificado.RFC;
            temporal.Telefono = modificado.Telefono;
            temporal.Correo = modificado.Correo;
            bool resultado = ActualizarArchivo();
            Clientes = LeerClientes();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Cliente item in Clientes)
            {
                datos += string.Format("{0}|{1}|{2}|{3}|{4}|{5}\n", item.Nombre, item.Apellido, item.Direccion, item.RFC, item.Telefono, item.Correo);
            }
            return archivoClientes.Guardar(datos);
        }
        public List<Cliente> LeerClientes()
        {
            string datos = archivoClientes.Leer();
            if (datos != null)
            {
                List<Cliente> clientee = new List<Cliente>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length - 1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Cliente a = new Cliente()
                    {
                        Nombre = campos[0],
                        Apellido = campos[1],
                        Direccion = campos[2],
                        RFC = campos[3],
                        Telefono = campos[4],
                        Correo = campos[5],
                    };
                    clientee.Add(a);
                }
                Clientes = clientee;
                return clientee;
            }
            else
            {
                return null;
            }
        }
    }
}
