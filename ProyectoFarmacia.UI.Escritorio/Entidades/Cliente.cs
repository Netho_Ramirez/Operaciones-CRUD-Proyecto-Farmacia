﻿using ProyectoFarmacia.UI.Escritorio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFarmacia.UI.Escritorio
{
    public class Cliente:Persona 
    {        
        public string Direccion { get; set; }
        public string RFC { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }    
    }
}
